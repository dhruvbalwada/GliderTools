Citing and Referencing
======================

[![DOI](https://zenodo.org/badge/141922866.svg)](https://zenodo.org/badge/latestdoi/141922866)


Reference
---------
If you would like to cite or reference Glider Tools, please use:

Link to DOI



Authors and contributors
------------------------

Authors and people who have contributed to the development. Not necessarily on the paper.
