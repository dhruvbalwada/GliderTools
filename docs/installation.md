Installation
============

Note that GliderTools has been written in Python 3.6 and extensively tested in Python 3.7.
Python 2 will is unfortunately not supported.

If you are new to Python, we recommend using the Anaconda distribution of Python 3.6+:
https://www.anaconda.com/distribution/


##### PyPI
To install the core package run: `pip install glidertools`.
Check that you are using the correct version of `pip`.

##### GitLab
For the most up to date version of GliderTools, you can install directly from the online repository hosted on GitLab.

1. Clone glidertools to your local machine: `git clone --depth 1 https://gitlab.com/socco/GliderTools` (--depth 1 reduces the download size)
2. Change to the parent directory of GliderTools
3. Install glidertools with `pip install -e ./GliderTools`. This will allow changes you make locally, to be reflected when you import the package in Python

##### Recommended, but optional packages
There are some non-essential packages that are not installed by default, as these are large packages or can result in installation errors, resulting in failure to install GliderTools. These should install automatically with `pip install <package_name>`:
 - `gsw`: accurate density calculation (installation may fail in some cases - seawater is used as rollback option)
 - `pykrige`: variogram plotting (installation generally works, except when bundled with GliderTools)
 - `plotly`: interactive 3D plots (large package)
